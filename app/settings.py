"""Settings configuration"""

import os
from dotenv import load_dotenv

load_dotenv()

ENV = os.getenv('FLASK_ENV', default = 'development')
DEBUG =  ENV == 'development'
PSQL_URI = 'postgres:///incident_tracker'
SECRET_KEY = os.getenv('SECRET_KEY', default='octocat')
